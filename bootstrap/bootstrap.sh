
read -p "reset cluster? (y/n) " yn

# https://github.com/rancher/k3d/issues/835
if [ "$yn" != "${yn#[Yy]}" ] ;then
  k3d cluster delete --config "./k3d-config.yaml"

  # sshfs bug doesnt like cluster restarts
  umount -f toor@homelab-1:/tmp
  sshfs toor@homelab-1:/tmp /tmp/test
  mkdir -p /tmp/test/k3d/kubelet/pods

  k3d cluster create --config "./k3d-config.yaml" 
fi

kubectl create namespace argocd
#helm repo add argo https://argoproj.github.io/argo-helm
#helm repo update
export ARGOCD_EXEC_TIMEOUT=300
helm upgrade --install argo-deploy argo/argo-cd -n argocd -f ../argocd/argo-values.yaml

NAME=$(kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2)
echo "waiting for pod $NAME"

while [[ $(kubectl get pods $NAME -n argocd -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; 
  do 
     sleep 60;
  done

#kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
#kubectl patch storageclass ceph-block -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
#kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
kubectl port-forward service/argo-deploy-argocd-server -n argocd 8080:80 &

#skubectl port-forward service/nextcloud-deploy -n nextcloud 8084:8080 & 
